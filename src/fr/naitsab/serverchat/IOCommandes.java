package fr.naitsab.serverchat;

import java.io.*;
import java.net.Socket;

public class IOCommandes
{
    // Variables
    private Socket socket;
    private BufferedReader lectureEcran;
    private PrintStream ecritureEcran;
    private BufferedReader lectureReseau;
    private PrintStream ecritureReseau;

    //Constructeur
    public IOCommandes(Socket socket)
    {
        this.socket = socket;
        this.lectureEcran = new BufferedReader(new InputStreamReader(System.in));
        this.ecritureEcran = new PrintStream(System.out);

        try { this.lectureReseau = new BufferedReader(new InputStreamReader(this.socket.getInputStream())); }
        catch (IOException e) { e.printStackTrace(); }

        try { this.ecritureReseau = new PrintStream(this.socket.getOutputStream()); }
        catch (IOException e) { e.printStackTrace(); }
    }

    // Fonctions
    public void ecrireEcran(String texte)
    {
        this.ecritureEcran.println(texte);
    }

    public String lireEcran()
    {
        try { return this.lectureEcran.readLine(); } catch (IOException e) { e.printStackTrace(); }
        return null;
    }

    public void ecrireReseau(String texte)
    {
        this.ecritureReseau.println(texte);
    }

    public String lireReseau()
    {
        try { return this.lectureReseau.readLine(); } catch (IOException e) { e.printStackTrace(); }
        return null;
    }
}
