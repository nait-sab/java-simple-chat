package fr.naitsab.serverchat;

import java.io.IOException;
import java.net.Socket;

public class ClientChat
{
    // Variables
    private Socket client;
    private IOCommandes ioCommandes;
    private String message;

    // USERS
    private boolean demander_liste = false;

    // Constructeur
    public ClientChat()
    {
        try { this.client = new Socket("172.16.1.161", 5999); } catch (IOException e) { e.printStackTrace(); }
        this.ioCommandes = new IOCommandes(this.client);
    }

    // Fonctions
    public void dialogueConsole()
    {
        while (true)
        {
            message = ioCommandes.lireReseau();
            ioCommandes.ecrireEcran(message);

            // Commande USER
            if (this.demander_liste)
            {
                String[] traitement = message.split(" ");
                int longueur = Integer.parseInt(traitement[0].replaceAll("OK:", ""));
                for (int compteur = 0; compteur < longueur; compteur++)
                {
                    message = ioCommandes.lireReseau();
                    ioCommandes.ecrireEcran(message);
                }
                this.demander_liste = false;
            }

            // Lire la saisi client
            message = ioCommandes.lireEcran();
            // Vérifier la commande
            if (message.equals("QUIT")) break;
            if (message.equals("USERS")) this.demander_liste = true;

            // Envoyer la saisi au serveur
            ioCommandes.ecrireReseau(message);
        }
    }
}

// Exercice 6

/*
        // 1 - Attendre la connection
        do { message = ioCommandes.lireReseau(); }
        while (!(message.startsWith("Bienvenue")));

        ioCommandes.ecrireEcran("Connection établie");
        ioCommandes.ecrireEcran("USER:<Nom>");

        // 2 - Créer le login
        do { message = ioCommandes.lireEcran(); }
        while (!(message.startsWith("USER:<")) && !(message.endsWith(">")));
        ioCommandes.ecrireReseau(message);
        message = ioCommandes.lireReseau();
        if (message.startsWith("OK:")) ioCommandes.ecrireEcran("Bienvenue sur le serveur");
        else if (message.startsWith("ERR:Ce login"))
            ioCommandes.ecrireEcran("Erreur::Ce login est déjà prit");
        else ioCommandes.ecrireEcran("Erreur::Impossible de vous connecter au serveur");
 */