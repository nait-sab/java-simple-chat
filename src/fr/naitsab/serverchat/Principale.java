package fr.naitsab.serverchat;

import java.io.IOException;
import java.net.Socket;

public class Principale
{
    public static void main(String[] args)
    {
        Socket socket;
        IOCommandes ioCommandes;
        String message;

        try {

            socket = new Socket("172.16.1.161", 5999);

            ioCommandes = new IOCommandes(socket);

            while (true)
            {
                // Lire la saisi client
                message = ioCommandes.lireEcran();

                // Vérifier la commande
                if (message.equals("QUIT")) break;

                // Envoyer la saisi au serveur
                ioCommandes.ecrireReseau(message);
                // Lire la réponse serveur
                message = ioCommandes.lireReseau();
                // Afficher la réponse serveur
                ioCommandes.ecrireEcran(message);
            }

        } catch (IOException e) { e.printStackTrace(); }
    }
}
